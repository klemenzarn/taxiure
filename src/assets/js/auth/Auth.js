class Auth {
    constructor() {
        this.provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                Event.fire('userSignedIn', user);
            } else {
                Event.fire('userSignedOut');
            }
        });
    }

    getSignedUser() {
        return firebase.auth().currentUser;
    }

    setProvider(provider) {
        this.provider = provider;
    }

    getProvider() {
        return this.provider;
    }

    signIn() {
        firebase.auth().signInWithPopup(this.getProvider()).then(function(result) {
            // This gives you a Google Access Token. You can use it to access the Google API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            console.log(token);
            console.log(user);
        }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            console.log(errorCode);
            console.log(errorMessage);
            console.log(email);
            console.log(credential);
        });
    }

    signOut() {
        firebase.auth().signOut().then(function() {
            console.log('sign out ok');
        }).catch(function(error) {
            console.log('sign out not ok', error);
        });
    }
}


export default Auth;