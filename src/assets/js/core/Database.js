var Database = (function() {

    var serializeData = function(workData) {
        var serialized = {
            id: workData.id,
            startDate: workData.startDate,
            endDate: workData.endDate,
            name: workData.name
        }

        for (let key in serialized) {
            if (serialized[key] == undefined) {
                delete serialized[key];
            }
        }

        if (workData.works != undefined) {
            serialized.works = [];
            workData.works.forEach(work => {
                serialized.works.push(serializeData(work));
            });
        }



        return serialized;
    };

    var getData = function(userId) {
        return new Promise((resolve, reject) => {
            firebase.database().ref('/users/' + userId).once('value').then(snapshot => {
                resolve(snapshot.val());
            }).catch(err => {
                reject(err);
            });
        });
    };

    var saveData = function(workData, userId) {
        return new Promise((resolve, reject) => {
            var sendingObject = serializeData(workData);

            firebase.database().ref('/users/' + userId + '/workData/' + workData.id).set(sendingObject).then(snapshot => {
                resolve(sendingObject);
            }).catch(err => {
                console.log(err);
                reject(err);
            });
        });
    };

    var createData = function(workData, userId) {
        return new Promise((resolve, reject) => {

            var newPostKey = firebase.database().ref().push().key;
            workData.id = newPostKey;

            var sendingObject = serializeData(workData);

            firebase.database().ref('/users/' + userId + '/workData/' + workData.id).set(sendingObject).then(snapshot => {
                console.log('SAVED');
                resolve(sendingObject);
            }).catch(err => {
                console.log(err);
                reject(err);
            });
        });
    };

    var getApiKeyData = function(apiKey) {
        return new Promise((resolve, reject) => {
            firebase.database().ref('/apiKeys/' + apiKey).once('value').then(snapshot => {
                resolve(snapshot.val());
            }).catch(err => {
                reject(err);
            });
        });
    };

    var setApiKey = function(userId, apiKey) {
        return new Promise((resolve, reject) => {
            firebase.database().ref('/users/' + userId + '/apiKey').set(apiKey).then(snapshot => {
                console.log('api key saved');
                resolve(apiKey);
            }).catch(err => {
                console.log(err);
                reject(err);
            });
        });
    };

    return {
        getData: getData,
        saveData: saveData,
        createData: createData,
        getApiKeyData: getApiKeyData,
        setApiKey: setApiKey
    }
})();


export default Database;