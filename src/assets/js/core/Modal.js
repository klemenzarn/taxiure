class Modal {
    constructor() {
        this.open = false;
        this.title = 'Dodaj novi delovni čas';
    }

    setTitle(title) {
        this.title = title;
    }

    show() {
        this.open = true;
    }

    hide() {
        this.open = false;
    }
}

export default Modal;