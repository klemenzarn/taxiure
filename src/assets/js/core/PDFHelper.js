import Database from './Database';
import WorkData from './WorkData';
import moment from 'moment';
import math from 'mathjs';

class PDFHelper {
    constructor() {
        this.workData = [];
    }

    export (month, year) {
        this.month = month;
        this.year = year;
        this.workData = [];

        let monthName = moment.months()[this.month];

        var dateThisMonth = moment().year(year).month(month).date(1);

        Database.getData()
            .then(workDataFromServer => {

                for (let work in workDataFromServer) {
                    var workData = workDataFromServer[work];

                    var workDataTemp = new WorkData(workData);
                    var parsedDate = moment(new Date(workDataTemp.startDate));
                    if (dateThisMonth.isSame(parsedDate, 'month')) {
                        this.workData.push(workDataTemp);
                    }
                }
                pdfMake.createPdf(this.createDocDefinition()).download('taxi ure-' + monthName + '-' + year + '.pdf');

            }).catch(err => {
                this.loadingData = false;
                console.log(err);
            });

    }

    createDocDefinition() {
        let monthName = moment.months()[this.month];

        var skupnoHours = {
            all: 0,
            effective: 0,
            regression: 0
        }

        var workDataDDArray = [];
        let index = 1;

        this.workData.sort((workA, workB) => {
            return moment(new Date(workA.startDate)).utc().diff(moment(new Date(workB.startDate)).utc())
        });

        this.workData.forEach(work => {
            let workingHours = work.getWorkingHours();
            skupnoHours.all += workingHours.all
            skupnoHours.effective += workingHours.effective
            skupnoHours.regression += workingHours.regression
            console.log(workingHours);

            //let workData = [];

            workDataDDArray.push({
                text: '(' + index + ') ' + moment(new Date(work.startDate)).format('dddd, D. MMMM, YYYY') + ', začetek: ' + work.getDateAndTime('startDate').time + ', konec: ' + work.getDateAndTime('endDate').time,
                style: ['bold', 'largeFont']
            });
            index++;
            workDataDDArray.push({
                text: 'Skupno ur: ' + math.round(workingHours.all, 2)
            });
            workDataDDArray.push({
                text: 'Efektivne ure: ' + math.round(workingHours.effective, 2)
            });
            workDataDDArray.push({
                text: 'Režijske ure: ' + math.round(workingHours.regression, 2)
            });
            workDataDDArray.push({
                text: '\nVožnje (' + work.works.length + '):',
                style: 'bold'
            });


            var table = {
                style: 'tableExample',
                table: {
                    body: [
                        ['Začetek', 'Konec']
                    ]
                },
                margin: [0, 0, 0, 30]

            }

            work.works.sort((workA, workB) => {
                return moment(new Date(workA.startDate)).utc().diff(moment(new Date(workB.startDate)).utc())
            });

            work.works.forEach(workElement => {
                table.table.body.push([
                    workElement.getReadableDate('startDate'),
                    workElement.getReadableDate('endDate')
                ]);
            });

            workDataDDArray.push(table);



        });

        var skupnoData = [];
        skupnoData.push({
            text: 'Skupni seštevek za mesec',
            style: ['bold', 'largerFont'],
            margin: [0, 10, 0, 0]
        });
        skupnoData.push({
            text: 'Skupno ur: ' + math.round(skupnoHours.all, 2)
        });
        skupnoData.push({
            text: 'Efektivne ure: ' + math.round(skupnoHours.effective, 2)
        });
        skupnoData.push({
            text: 'Režijske ure: ' + math.round(skupnoHours.regression, 2)
        });
        skupnoData.push({
            text: 'Seznam voženj (' + this.workData.length + '): ',
            style: 'largerFont',
            margin: [0, 20, 0, 0]
        });

        workDataDDArray.unshift(skupnoData);

        var docDefinition = {
            content: [
                { text: 'Izpis delovnih ur za mesec ' + monthName + ' ' + this.year, style: 'header' },
                '',
                // { text: 'Another text', style: 'anotherStyle' },
                //{ text: 'Multiple styles applied', style: ['header', 'anotherStyle'] },

            ],

            styles: {
                header: {
                    fontSize: 24,
                    bold: true
                },
                bold: {
                    bold: true
                },
                largeFont: {
                    fontSize: 16,
                },
                largerFont: {
                    fontSize: 18,
                },
                anotherStyle: {
                    italic: true,
                    alignment: 'right'
                }
            }
        };

        docDefinition.content.push(workDataDDArray)

        return docDefinition;
    }


}


export default PDFHelper;