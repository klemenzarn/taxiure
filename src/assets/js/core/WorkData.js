import moment from 'moment';

class WorkData {

    constructor(data) {
        if (data != undefined) {
            this.id = data.id;
            this.startDate = data.startDate;
            this.endDate = data.endDate;
            this.works = [];
            this.name = data.name;
            if (data.works != undefined) {
                data.works.forEach((work) => {
                    this.works.push(new WorkData(work));
                });
            }


        } else {

        }

    }

    save(userId) {
        return new Promise((resolve, reject) => {
            if (this.id == undefined) {
                Database.createData(this, userId)
                    .then(createdWorkData => {
                        resolve(createdWorkData);
                    }).catch(err => {
                        reject(err);
                    });
            } else {
                Database.saveData(this, userId)
                    .then(updatedWorkData => {
                        resolve(updatedWorkData);
                    }).catch(err => {
                        reject(err);
                    });
            }


        });

    }

    getWorkingHours() {
        //if (this.works != null && this.works != undefined && this.works.length > 0) {
        //gre za parent work
        var startingDate = moment(new Date(this.startDate));
        var endingDate = moment(new Date(this.endDate));

        var duration = moment.duration(endingDate.diff(startingDate));
        var hours = Math.round(duration.asHours() * 100) / 100;

        var effectiveHours = 0;
        if (this.works != null && this.works != undefined && this.works.length > 0) {
            this.works.forEach(drive => {
                effectiveHours += drive.getWorkingHours().all;
            });
        }

        return {
            all: hours,
            effective: effectiveHours,
            regression: hours - effectiveHours
        }
    }

    getDriveCount() {
        return this.works.length;

    }

    getReadableDate(dateName) {
        return moment(new Date(this[dateName])).format('DD.MM.YYYY HH:mm');
    }

    getDateAndTime(dateName) {
        return {
            date: moment(new Date(this[dateName])).format('DD.MM.YYYY'),
            time: moment(new Date(this[dateName])).format('HH:mm')
        };
    }

}


export default WorkData;