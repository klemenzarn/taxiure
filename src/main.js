import Vue from 'vue';
import App from './App.vue';
import axios from 'axios';

Vue.prototype.$http = axios;

window.Event = (function() {
    var vue = new Vue();

    var fireEvent = function(event, data) {
        data = data || null;
        vue.$emit(event, data);
    };

    var listenEvent = function(event, callback) {
        vue.$on(event, callback);
    };

    return {
        fire: fireEvent,
        listen: listenEvent
    }

})();

new Vue({
    el: '#app',
    render: h => h(App)
});